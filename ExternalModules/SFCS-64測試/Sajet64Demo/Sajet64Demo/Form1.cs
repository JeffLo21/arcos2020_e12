﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace FileWatch
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                bool start = Sajet.Sajet.SajetTransStart();
                if (start)
                {
                    MessageBox.Show("connect sajet success!");
                }
                else
                {
                    MessageBox.Show("connect sajet fail!");
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show($"fail! {ex.Message}");
            }
            
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("10008744");
                int len = sb.Length;
                bool trans = Sajet.Sajet.SajetTransData(1, sb, ref len);

                if (trans)
                {
                    MessageBox.Show($"transfer success! server response: {sb.ToString(0, len)}");
                }
                else
                {
                    MessageBox.Show("transfer fail!");
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show($"transfer fail! {ex.Message}");
            }

            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                bool close = Sajet.Sajet.SajetTransClose();
                if (close)
                {
                    MessageBox.Show("close sajet success!");
                }
                else
                {
                    MessageBox.Show("close sajet fail!");
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show($"close sajet fail! {ex.Message}");
            }
            
        }
    }
}
